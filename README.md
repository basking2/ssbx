# Ssbx

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'ssbx'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install ssbx

## Usage

Edit `~/.ssbx.yaml` to contain...

```yaml
user: You
pass: A secret password
# This is optional if you want to override $EDITOR.
editor: vi
```

Then use `ssbx` to view or edit a file.

If you set the password to `prompt` then you will be prompted for your password. This is highly preferred.

## Simple Recipes

    # Add a user with a simple password.
    ssbx -f f1.enc -a user1:userpass
    ssbx -f a -u user1 --passwd

    # ... or
    cat f1.enc | ssbx -f - -a user1:userpass > f2.enc

    # Remove the user.
    ssbx -f f1.enc -d user1:userpass

    # File data to standard out.
    ssbx -f f1.enc -o -

    # Edit file data and re-encrypt the file.
    ssbx -f f1.enc -e

    # Rotate passwords.
    # - Edit ~/.ssbx.yaml to store the new password.
    ssbx -f a -u user1 --passwd


## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).


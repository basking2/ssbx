module Ssbx
    module Util
        def self.read(istream)
            if istream.is_a? String
                stream = if istream == '-'
                    STDIN
                else
                    ::File.open(istream, 'rb')
                end

                if block_given?
                    begin
                        yield stream
                    ensure
                        stream.close
                    end
                else
                    stream
                end
            else
                raise Exception.new("Unsupported type #{istream.class}.")
            end
        end

        def self.write(ostream)
            if ostream.is_a? String
                stream = if ostream == '-'
                    STDOUT
                else
                    ::File.open(ostream, 'wb')
                end

                if block_given?
                    begin
                        yield stream
                    ensure
                        stream.close
                    end
                else
                    stream
                end
            else
                raise Exception.new("Unsupported type #{ostream.class}.")
            end
        end
    end
end

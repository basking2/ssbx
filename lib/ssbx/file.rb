module Ssbx

    # How data gets to disk and back.
    class File
        # Key tuples.
        #
        # This is an array of arrays.
        # The inner-arrays are key-tuples.
        # They consist of an id represented as a string,
        # a public key, an ecrypted private key, and the 
        # decryption key for the data ecrypted with the public key.
        #
        # This means you need to decrypt the private key to decrypt the data key.
        attr_accessor :keys

        # The file data.
        attr_accessor :data

        def initialize(optional_stream = nil)
            @keys = []
            @data = ''
            read(optional_stream) if optional_stream            
        end

        def write(out)
            # How many key records.
            out.write([@keys.length].pack('N'))
            @keys.each do |key_rec|
                # Write the key record columns.
                out.write([key_rec.length].pack('N'))

                key_rec.each do |col|
                    out.write([col.length].pack('N'))
                    out.write(col)
                end
            end

            out.write([@data.length].pack('N'))
            out.write(@data)
        end

        def read(istream)
            @keys = []
            # Read the key record length...
            istream.read(4).unpack('N')[0].times do
                @keys << []
                # Read the key record length...
                istream.read(4).unpack('N')[0].times do
                    col_len = istream.read(4).unpack('N')[0]
                    @keys[-1] << istream.read(col_len)
                end
            end

            sz = istream.read(4).unpack('N')[0]
            @data = istream.read(sz)
        end

    end
end
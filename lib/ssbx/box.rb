require 'ssbx/file'

require 'openssl'

module Ssbx

    # This is the encryption logic.
    # This uses File to read and write data, but the actual encryption logic is in here.
    class Box

        # The symetric cipher name used for the symetric parts of this algorithm.
        SYMETRIC_CIPHER_NAME = 'AES-256-CBC'

        # Create a new box.
        def initialize(file)
            if file.is_a? IO
                @file = File.new
                @file.read(file)
            elsif file.is_a? Ssbx::File
                @file = file
            else
                raise Exception.new("Unsupported input type #{file.class}. Try Ssbx::File or IO.")
            end
        end

        # Write the data given the userid and password.
        # If the user is not in the file, they are added.
        def write(outstream, userid, password, data)
            userrecord = @file.keys.select { |k| k[0] == userid }

            # Find or get our user record.
            if userrecord.length == 0 
                privkey = OpenSSL::PKey::RSA.new(2048)

                userrecord = [ userid ]
                userrecord << privkey.to_pem(OpenSSL::Cipher.new(SYMETRIC_CIPHER_NAME), password)
                userrecord << privkey.public_key.to_pem
                userrecord << '' # Private Key
                userrecord << '' # Initialization Vector.

                @file.keys << userrecord
            else
                userrecord = userrecord[0]

                privkey = OpenSSL::PKey::RSA.new(2048)

                # Rotate user record entries.
                userrecord[1] = privkey.to_pem(OpenSSL::Cipher.new(SYMETRIC_CIPHER_NAME), password)
                userrecord[2] = privkey.public_key.to_pem
                userrecord[3] = '' # Private Key
                userrecord[4] = '' # Initialization Vector.
            end

            # Make semetric cipher.
            cipher = OpenSSL::Cipher.new(SYMETRIC_CIPHER_NAME)
            cipher.encrypt

            key = cipher.random_key
            iv = cipher.random_iv

            # Encrypt data with key.
            ciphertext = cipher.update(data)
            ciphertext += cipher.final
            @file.data = ciphertext

            # Now update all key records with the new key and iv.
            @file.keys.each do |key_rec|
                # k[0 id, 1 priv, 2 pub, 3 key, 4 iv]
                k = OpenSSL::PKey::RSA.new(key_rec[2])
                key_rec[3] = k.public_encrypt(key)
                key_rec[4] = iv
            end

            @file.write(outstream)
        end

        # Decrypt the data user the given user ID and password.
        def read(instream, userid, password)
            @file.read(instream)
            userrecord = @file.keys.select { |k| k[0] == userid }

            # Find or get our user record.
            if userrecord.length == 0 
                raise Exception.new("User #{userid} not found in the file.")
            else
                userrecord = userrecord[0]
            end

            privkey = OpenSSL::PKey::RSA.new(userrecord[1], password)

            cipher = OpenSSL::Cipher.new(SYMETRIC_CIPHER_NAME)
            cipher.decrypt

            cipher.key = privkey.private_decrypt(userrecord[3])
            cipher.iv = userrecord[4]

            data = cipher.update(@file.data)
            data += cipher.final

            data
        end

        # Remove a user from the memory representation of this file.
        # You must do a valid write of the file to force credential rotation and remove the user
        # from the file on disk.
        def remove_user(user)
            @file.keys.reject! { |k| k[0] == user }
        end

        def list
            @file.keys.map { |r| r[0] }
        end

    end
end
require 'io/console'

module Ssbx
    module Cli
        def self.get_password(config)
            if config['pass'] == 'prompt'
                print "Enter your password: "
                STDIN.echo = false
                begin
                    config['pass'] = STDIN.readline.chomp
                    puts ''
                ensure
                    STDIN.echo = true
                end
            end

            config['pass']
        end
    end
end
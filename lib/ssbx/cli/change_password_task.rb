require 'io/console'

require 'ssbx/file'
require 'ssbx/box'
require 'ssbx/util'

module Ssbx
    module Cli
        def self.change_password(config)
            STDIN.echo = false
            begin
                print("Old password: ")
                old_pw = STDIN.readline.chomp
                print("\nNew password: ")
                new_pw1 = STDIN.readline.chomp
                print("\nRetype new password: ")
                new_pw2 = STDIN.readline.chomp
                puts ''
        
                if new_pw1 != new_pw2
                    puts "Old and new passwords do not match. Taking no action."
                else
                    f = Ssbx::File.new
                    bx = Ssbx::Box.new(f)
        
                    # Decrypt with the old key.
                    data = Ssbx::Util.read(config['file']) { |io| bx.read(io, config['user'], old_pw) }
        
                    Ssbx::Util.write(config['file']) { |io| bx.write(io, config['user'], new_pw1, data) }
        
                end
            ensure
                STDIN.echo = true
            end
        end
    end
end
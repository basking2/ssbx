require 'ssbx/file'
require 'ssbx/box'
require 'ssbx/cli/get_password_task'

module Ssbx
    module Cli
        def self.list_users(config)
            f = Ssbx::File.new
            bx = Ssbx::Box.new(f)
        
            Ssbx::Util.read(config['file']) do |io|
                f.read(io)
            end
        
            bx.list.each do |u|
                puts u
            end           
        end

        def self.delete_users(config)
            config['delete_users'].each do |u|
                f = Ssbx::Util.read(config['file']) { |io| Ssbx::File.new(io) }
                bx = Ssbx::Box.new(f)
                bx.remove_user(u)
                Ssbx::Util.write(config['file']) { |io| f.write(io) }
            end
        end
        
        def self.add_users(config)
            f = Ssbx::File.new
            bx = Ssbx::Box.new(f)
            p = get_password(config)
            
            data = Ssbx::Util.read(config['file']) do |io|
                bx.read(io, config['user'], p)
            end
        
            config['add_users'].each do |u|
                u, p = u[0], u[1] || u[0]
                
                Ssbx::Util.write(config['file']) do |io|
                    bx.write(io, u, p, data)
                end 
            end
        end
        
    end
end
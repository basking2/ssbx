require 'ssbx/file'
require 'ssbx/box'
require 'ssbx/cli/get_password_task'
require 'tempfile'
module Ssbx
    module Cli

        def self.set(config)
            f = Ssbx::File.new
            bx = Ssbx::Box.new(f)
            data = Ssbx::Util.read(config['set']) do |io|
                io.read
            end
            Ssbx::Util.write(config['file']) do |io|
                bx.write(io, config['user'], get_password(config), data)
            end 
        end

        def self.get(config)
            f = Ssbx::File.new
            bx = Ssbx::Box.new(f)
            data = Ssbx::Util.read(config['file']) do |io|
                bx.read(io, config['user'], get_password(config))
            end
        
            Ssbx::Util.write(config['out']) { |io| io.write(data) }
        end

        def self.edit(config)
            f = Ssbx::File.new
            bx = Ssbx::Box.new(f)
            p = get_password(config)
        
            data = if ::File.file? config['file']
                Ssbx::Util.read(config['file']) do |io|
                    bx.read(io, config['user'], p)
                end
            else
                ''
            end
        
            tmpfile = Tempfile.new("ssbx")
            tmpfile.write(data)
            tmpfile.flush
        
            begin
                editor = config['editor'] || ENV['EDITOR']
        
                system("#{editor} #{tmpfile.path}")
        
                tmpfile.rewind
                data = ::File.read(tmpfile)
                ::File.open(config['file'], 'wb') do |io|
                    bx.write(io, config['user'], p, data)
                end
            ensure
                tmpfile.unlink
            end        
        end
    end
end
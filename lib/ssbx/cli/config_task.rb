require 'io/console'
require 'yaml'

module Ssbx
    module Cli

        @@default_config_file = ::File.join(ENV['HOME'], '.ssbx.yaml')

        def self.load_default(config)
            if ::File.file? @@default_config_file
                c = YAML::load(::File.read(@@default_config_file))
                config.merge!(c)
            end
        end

        def self.config(config)
            print "User name? [#{ENV['USER']}]: "
            config['user'] = STDIN.readline.chomp
            config['user'] = ENV['USER'] if config['user'] == ''
        
            require 'io/console'
            STDIN.echo = false
            print "Password: "
            p = begin
                STDIN.readline.chomp
            ensure
                STDIN.echo = true
                puts ''
            end
            config['pass'] = p unless p == ''
        
            print "Editor? [#{ENV['EDITOR']}]: "
            e = STDIN.readline.chomp
            config['editor'] = e unless e == ''
        
            require 'yaml'
            ::File.open(@@default_config_file, 'wb') do |io|
                io.write(YAML::dump(
                    config.select do |k,v|
                        k !~ /^delete_users|add_users|config|verbose$/
                    end
                ))
            end
        end
    end
end
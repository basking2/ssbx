require "test_helper"

require 'tempfile'

class SsbxTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Ssbx::VERSION
  end

  def test_file_read_write
    f1 = Ssbx::File.new()
    f2 = Ssbx::File.new()

    f1.keys << [ 'a', 'b', 'hello' ]
    f1.keys << [ 'world', 'd', 'woohoo' ]

    f1.data = "This is file data."

    tmpf = Tempfile.new('foo')

    begin
      f1.write(tmpf)
      tmpf.rewind
      f2.read(tmpf)

      assert_equal f1.keys[0], f2.keys[0]
      refute_equal f1.keys[0], f2.keys[1]
      assert_equal f1.keys[1], f2.keys[1]
      assert_equal f1.data, f2.data
    ensure
      tmpf.close
      tmpf.unlink
    end

  end


  def test_box_read_write

    tmpf = Tempfile.new('foo')

    begin
      f = Ssbx::File.new()
      bx = Ssbx::Box.new(f)
      bx.write(tmpf, "user1", "password1", 'This is the file data.')

      f = Ssbx::File.new()
      bx = Ssbx::Box.new(f)
      tmpf.rewind
      d = bx.read(tmpf, "user1", "password1")

      assert_equal 'This is the file data.', d
    ensure
      tmpf.close
      tmpf.unlink
    end

  end

  def test_box_user1_can_open_user2_data

    tmpf = Tempfile.new('foo')

    begin
      f = Ssbx::File.new()
      bx = Ssbx::Box.new(f)

      # First write user1 data, so they are in the file.
      bx.write(tmpf, "user1", "password1", 'This is the file data.')
      tmpf.rewind

      # Now write user2 data. This should be accessible by user1.
      bx.write(tmpf, "user2", "password2", 'This is the file data2.')
      tmpf.rewind

      f = Ssbx::File.new()
      bx = Ssbx::Box.new(f)

      # User2 and password2 decrypt.
      tmpf.rewind
      d = bx.read(tmpf, "user2", "password2")
      assert_equal d, 'This is the file data2.'

      # User1 and password1 decrypt.
      tmpf.rewind
      d = bx.read(tmpf, "user1", "password1")
      assert_equal d, 'This is the file data2.'


      # User1 and password2 fail.
      tmpf.rewind
      assert_raises OpenSSL::PKey::RSAError do
        d = bx.read(tmpf, "user1", "password2")
      end
    ensure
      tmpf.close
      tmpf.unlink
    end
  end

  def test_box_list

    tmpf = Tempfile.new('foo')

    begin
      f = Ssbx::File.new()
      bx = Ssbx::Box.new(f)
      bx.write(tmpf, "user1", "password1", 'This is the file data.')
      tmpf.rewind
      bx.write(tmpf, "user2", "password1", 'This is the file data.')
      tmpf.rewind

      assert_equal 'user1', bx.list[0]
      assert_equal 'user2', bx.list[1]

      tmpf.rewind
      f = Ssbx::File.new()
      f.read(tmpf)

      bx = Ssbx::Box.new(f)
      assert_equal 'user1', bx.list[0]
      assert_equal 'user2', bx.list[1]

      # Now try removing user2.
      tmpf.rewind
      bx.remove_user('user2')
      bx.write(tmpf, "user1", "password1", 'This is the file data.')
      tmpf.rewind
      f = Ssbx::File.new()
      f.read(tmpf)
      bx = Ssbx::Box.new(f)
      assert_equal 1, bx.list.length

      assert_equal 'user1', bx.list[0]

    ensure
      tmpf.close
      tmpf.unlink
    end

  end

end
